function z(n) {
  if (n < 10) {
    return "0" + n;
  }

  return "" + n;
}

let version = process.env["CI_COMMIT_SHORT_SHA"] || "0";

let build = (process.env["CI_PIPELINE_ID"] || "0") + "-" + version;

let now = new Date();

let y = now.getUTCFullYear() - 2000;
let m = now.getUTCMonth() + 1;
let d = now.getUTCDate();

let th = now.getUTCHours();
let tm = now.getUTCMinutes();
let ts = now.getUTCSeconds();

let timestamp = "" + y + z(m) + z(d) + "-" + z(th) + z(tm) + z(ts);

let obj = {
  epoch: "" + ((now.getTime() / 1000 / 86400).toFixed() - 18178),
  version: version,
  build: build,
  timestamp: timestamp,
};

console.log(JSON.stringify(obj, undefined, 2));
