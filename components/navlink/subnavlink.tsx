import * as React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import clsx from "clsx";
import type { SubNavLink as ISubNavLink } from "types";

interface Props {
  subNavLink: ISubNavLink;
  onClick: () => void;
  expandLabel: () => void;
  isCollapsed: boolean;
}
export const SubNavLink: React.FC<Props> = ({
  subNavLink,
  onClick,
  isCollapsed,
  expandLabel,
}) => {
  const { label, url } = subNavLink;
  const router = useRouter();
  const [isActive, setIsActive] = React.useState(router.pathname === url);

  React.useEffect(() => {
    setIsActive(router.pathname === url);
  }, [router.pathname]);

  React.useEffect(() => {
    if (isActive) {
      expandLabel();
    }
  }, [isActive]);

  if (isCollapsed) {
    return null;
  }

  return (
    <Link href={url}>
      <a
        className={clsx("tw-sub-nav-link", {
          "tw-sub-nav-link-active": isActive,
        })}
        onClick={onClick}
      >
        {label}
      </a>
    </Link>
  );
};
