import { useRouter } from "next/router";
import * as React from "react";
import { Scrollbars } from "react-custom-scrollbars";
import { TableOfContent } from "../../types";
import { TocLink } from "../toclink/toclink";
import clsx from "clsx";
import style from "./tableOfContents.module.scss";

interface Props {
  isOpenAtMobile: boolean;
}
export const TableOfContents: React.FC<Props> = ({ isOpenAtMobile }) => {
  const router = useRouter();
  const [toc, setToc] = React.useState<TableOfContent[]>([]);

  React.useEffect(() => {
    const _toc: TableOfContent[] = [];
    const headings = Array.from(
      document
        .querySelectorAll(
          ".markdown > h1.toc, .markdown > h2.toc, .markdown > h3.toc, .markdown > h4.toc"
        )
        .values()
    );
    for (let heading of headings) {
      _toc.push({
        depth: Number(heading.tagName[1]),
        slug: heading.id,
        value: (heading as HTMLElement).innerText,
      });
    }
    setToc(_toc);
  }, [router.asPath]);

  return (
    <div
      className={clsx(
        style.tableOfContents,
        isOpenAtMobile && style.tableOfContentsMobile
      )}
    >
      <Scrollbars
        autoHide
        autoHideTimeout={1000}
        autoHideDuration={200}
        thumbMinSize={30}
        universal={true}
        style={{ width: "100%", height: "100%" }}
      >
        <div
          className={`font-bold ${
            isOpenAtMobile ? "text-xl" : "text-sm"
          } leading-wide mt-6 mb-4 pl-4`}
        >
          Contents
        </div>
        {toc.map((t, i) => (
          <TocLink isMobile={isOpenAtMobile} item={t} key={`${t.value}-${i}`} />
        ))}
      </Scrollbars>
    </div>
  );
};
