export interface Meta {
  order: number;
  title: string;
  sideTitle: string;
}

export interface DocPageMeta extends Meta {
  slug: string[];
  filePath: string;
}

export interface TableOfContent {
  depth: number;
  value: string;
  slug: string;
}

export interface DocPageData extends DocPageMeta {
  markdown: string;
  toc: TableOfContent[];
}

export type NavigationLinkType = "page" | "folder" | "folder-page";

export interface NavigationLink extends DocPageMeta, Meta {
  type: NavigationLinkType;
  children: DocPageMeta[];
}

type Without<T, U> = { [P in Exclude<keyof T, keyof U>]?: never };
type XOR<T, U> = T | U extends object
  ? (Without<T, U> & U) | (Without<U, T> & T)
  : T | U;

interface NavLinkCommon {
  label: string;
  title?: string;
}
interface NavLinkExpandable {
  expanded?: boolean;
}
interface NavLinkWithUrl extends NavLinkCommon, NavLinkExpandable {
  url: string;
}
interface NavLinkWithSubitems extends NavLinkCommon, NavLinkExpandable {
  subitems: SubNavLink[];
}
export type NavLink = XOR<NavLinkWithUrl, NavLinkWithSubitems>;
export interface SubNavLink extends NavLinkCommon {
  url: string;
}

export interface NextPrevData {
  label: string;
  url: string;
}
