module.exports = {
  siteUrl: process.env.SITE_URL || "https://FIREBASE_PROJECT_TEST.web.app",
  generateRobotsTxt: true,
  exclude: [
    '/style-guide',
    '/guides/summary',
  ]
};
