import * as React from "react";
import { TabButtons } from "./tabButtons";

export const Tabs: React.FC = ({ children }) => {
  const [activeTab, setActiveTab] = React.useState(
    (children as any)[0].props.label
  );

  let content;
  let buttons: any[] = [];

  React.Children.map(children, (child, index) => {
    buttons.push((child as any).props.label);
    if ((child as any).props.label === activeTab)
      content = (child as any).props.children;
  });

  return (
    <div>
      <TabButtons
        activeTab={activeTab}
        buttons={buttons}
        changeTab={setActiveTab}
      />
      <div className="tab-content">{content}</div>
    </div>
  );
};
