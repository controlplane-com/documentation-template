import * as React from "react";
import { TableOfContent } from "types";
import clsx from "clsx";
import Link from "next/link";
import { useRouter } from "next/router";

interface Props {
  item: TableOfContent;
  isMobile: boolean;
}
export const TocLink: React.FC<Props> = ({ item, isMobile }) => {
  const router = useRouter();

  const [isActive, setIsActive] = React.useState(false);

  React.useEffect(() => {
    const onScroll = () => {
      const res = window.location.toString().endsWith(`#${item.slug}`);
      setIsActive(res);
    };
    document.addEventListener("scroll", onScroll);
    return () => {
      document.removeEventListener("scroll", onScroll);
    };
  }, []);

  return (
    <Link href={`${router.asPath.split("#")[0]}#${item.slug}`}>
      <a
        className={clsx("tw-toc-link", {
          "tw-toc-depth-1": item.depth === 1,
          "tw-toc-depth-2": item.depth === 2,
          "tw-toc-depth-3": item.depth === 3,
          "tw-toc-depth-4": item.depth === 4,
          "tw-toc-link-active": isActive,
          "tw-toc-link-mobile": isMobile,
        })}
      >
        {item.value}
      </a>
    </Link>
  );
};
