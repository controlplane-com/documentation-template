import { useRouter } from "next/router";
import * as React from "react";
import { NextPrevData } from "types";
import { sidebar } from "./../sidebar";
import { NextDoc } from "./nextDoc";
import { PreviousDoc } from "./previousDoc";

export const NextPrevButtons: React.FC = () => {
  const router = useRouter();

  const [prev, setPrev] = React.useState<NextPrevData | null>(null);
  const [next, setNext] = React.useState<NextPrevData | null>(null);

  React.useEffect(() => {
    const pathname = router.pathname;
    let currentUrl = "";
    let previous: NextPrevData | null = null;
    let current: NextPrevData | null = null;
    let next: NextPrevData | null = null;

    const normalized: NextPrevData[] = [];
    for (let navLink of sidebar) {
      if (navLink.url) {
        normalized.push({ url: navLink.url, label: navLink.label });
      }
      if (navLink.subitems) {
        for (let subnavLink of navLink.subitems) {
          normalized.push({ url: subnavLink.url, label: subnavLink.label });
        }
      }
    }

    for (let link of normalized) {
      if (current && next) {
        break;
      }

      if (!link.url) {
        continue;
      }
      if (link.url == pathname) {
        current = link;
        currentUrl = link.url;
      }
      if (!current) {
        previous = link;
      }
      if (current && currentUrl !== link.url) {
        next = link;
      }
    }

    setPrev(previous);
    setNext(next);
  }, [router.asPath]);

  return (
    <div className="tw-next-prev">
      <PreviousDoc link={prev} />
      <NextDoc link={next} />
    </div>
  );
};
