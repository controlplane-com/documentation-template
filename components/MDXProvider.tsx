import React from "react";
import { MDXProvider } from "@mdx-js/react";
import { PrismCodeHighlighter } from "./prismCodeHighlighter";
import { Link as LinkIcon, AlignRight } from "react-feather";
import clsx from "clsx";
import {
  AlertCircle,
  AlertTriangle,
  Info,
  Flag,
  Bookmark,
} from "react-feather";
import { Tabs } from "./tabs/tabs";
import Link from "next/link";

const mdComponents: {
  h1: React.FC;
  h2: React.FC;
  h3: React.FC;
  h4: React.FC;
  h5: React.FC;
  h6: React.FC;
  admonition: React.FC<any>;
  video: React.FC<any>;
  cplncode: React.FC<any>;
  uplink: React.FC<any>;
  tabs: React.FC<any>;
  code: React.FC<any>;
} = {
  tabs: (props) => {
    return <Tabs {...props} />;
  },
  uplink: (props) => {
    return (
      <div className="flex justify-end items-center">
        <a href={props.to}>
          <kbd>↑</kbd>
        </a>
      </div>
    );
  },
  admonition: (props) => {
    return (
      <div className={clsx("tw-admonition", `tw-admonition-${props.type}`)}>
        <InfoPrefix type={props.type} />
        {props.children}
      </div>
    );
  },
  video: (props) => {
    return <video {...props}>{props.children}</video>;
  },
  cplncode: (props) => {
    const ref = React.useRef<HTMLDivElement>(null as any);
    const [isCopied, setIsCopied] = React.useState(false);
    const noCopy = props.noCopy || false;

    React.useEffect(() => {
      let timeout: any = null as any;
      if (isCopied) {
        timeout = setTimeout(() => {
          setIsCopied(false);
          timeout = null;
        }, 2000);
      }

      return () => {
        if (timeout) {
          clearTimeout(timeout);
        }
      };
    }, [isCopied]);

    return (
      <>
        {props.title ? (
          <div
            className="font-bold border-b border-gray-300 whitespace-normal	tw-sfmono"
            style={{
              padding: 16,
              color: "rgb(57, 58, 52)",
              backgroundColor: "rgb(246, 248, 250)",
              fontSize: 13.333,
            }}
          >
            {props.title}
          </div>
        ) : null}
        <div className={clsx(`tw-admonition-cplncode`)}>
          {noCopy ? null : (
            <div
              className="copy absolute bg-gray-600 text-gray-100 px-2 py-1 text-xs rounded cursor-pointer uppercase tracking-wider"
              style={{ right: 15, top: 15 }}
              onClick={() => {
                let txt = "";
                if (ref.current) {
                  txt = ref.current.innerText;
                }
                if (typeof window !== "undefined") {
                  window.navigator.clipboard.writeText(txt);
                }
                setIsCopied(true);
              }}
            >
              {isCopied ? "copied" : "copy"}
            </div>
          )}
          <div ref={ref}>{props.children}</div>
        </div>
      </>
    );
  },
  h1: (props) => {
    const { id, content, excluded } = getHeadingIdContent(props);
    return (
      <h1 {...props} id={id} className={`relative ${!excluded && "toc"}`}>
        <span className="inline-block" style={{ width: "85%" }}>
          {content}
        </span>
        <Link href={`#${id}`}>
          <a className="ml-1 text-gray-600 tw-heading-link" />
        </Link>
        <AlignRight
          className="absolute cursor-pointer mobile-toc"
          style={{
            right: 10,
          }}
        />
      </h1>
    );
  },
  h2: (props) => {
    const { id, content, excluded } = getHeadingIdContent(props);
    return (
      <h2 {...props} id={id} className={`${!excluded && "toc"}`}>
        {content}
        <Link href={`#${id}`}>
          <a className="ml-1 text-gray-600 tw-heading-link">
            <LinkIcon className="tw-heading-link-icon" />
          </a>
        </Link>
      </h2>
    );
  },
  h3: (props) => {
    const { id, content, excluded } = getHeadingIdContent(props);
    return (
      <h3 {...props} id={id} className={`${!excluded && "toc"}`}>
        {content}
        <Link href={`#${id}`}>
          <a className="ml-1 text-gray-600 tw-heading-link">
            <LinkIcon className="tw-heading-link-icon" />
          </a>
        </Link>
      </h3>
    );
  },
  h4: (props) => {
    const { id, content, excluded } = getHeadingIdContent(props);
    return (
      <h4 {...props} id={id} className={`${!excluded && "toc"}`}>
        {content}
        <Link href={`#${id}`}>
          <a className="ml-1 text-gray-600 tw-heading-link">
            <LinkIcon className="tw-heading-link-icon" />
          </a>
        </Link>
      </h4>
    );
  },
  h5: (props) => {
    const { id, content } = getHeadingIdContent(props);
    return (
      <h5 {...props} id={id}>
        {content}
        <Link href={`#${id}`}>
          <a className="ml-1 text-gray-600 tw-heading-link">
            <LinkIcon className="tw-heading-link-icon" />
          </a>
        </Link>
      </h5>
    );
  },
  h6: (props) => {
    const { id, content } = getHeadingIdContent(props);
    return (
      <h6 {...props} id={id}>
        {content}
        <Link href={`#${id}`}>
          <a className="ml-1 text-gray-600 tw-heading-link">
            <LinkIcon className="tw-heading-link-icon" />
          </a>
        </Link>
      </h6>
    );
  },
  code: (props) => <PrismCodeHighlighter {...props} />,
};

const MyProvider: React.FC = ({ children }) => (
  <MDXProvider components={mdComponents}>{children}</MDXProvider>
);

export default MyProvider;

function InfoPrefix({ type }: { type: string }) {
  return (
    <div className="flex items-center mb-4">
      {type === "info" ? (
        <AlertCircle />
      ) : type === "note" ? (
        <Info />
      ) : type === "caution" ? (
        <AlertTriangle />
      ) : type === "warning" ? (
        <Flag />
      ) : (
        <Bookmark />
      )}
      <span className="ml-2 font-bold">{type.toUpperCase()}</span>
    </div>
  );
}

function getHeadingIdContent(props: any) {
  let id = "";
  try {
    id = (props.children as string).split("{")[1].split("}")[0].slice(1);
  } catch (e) {
    id = "";
  }
  let content = props.children;
  try {
    content = (props.children as string).split("{")[0].trim();
  } catch (e) {
    content = props.children;
  }
  let excludeFromToC = props.children.includes("--exclude");
  return { id, content, excluded: excludeFromToC };
}
