import { NavLink } from "types";

export const sidebar: NavLink[] = [
  { label: "FIRST PAGE", url: "/style-guide" },
  {
    label: "Sample Folder",
    subitems: [
      {
        label: "Style Guide in Folder",
        url: "/sample_folder/style-guide",
      },
    ],
    expanded: false,
  },
  { label: 'Test MDX Format', url: '/mdx-test/mdx-format' },
  { label: 'Test TSX Format', url: '/mdx-test/tsx-format' },
];
