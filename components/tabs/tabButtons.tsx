import * as React from "react";
import clsx from "clsx";

interface Props {
  buttons: any[];
  changeTab: (tab: string) => void;
  activeTab: string;
}
export const TabButtons: React.FC<Props> = ({
  buttons,
  changeTab,
  activeTab,
}) => {
  return (
    <div className="mb-8">
      {buttons.map((button) => {
        return (
          <button
            key={button}
            className={clsx("tw-tab", {
              "tw-tab-active": button === activeTab,
            })}
            onClick={() => changeTab(button)}
          >
            {button}
          </button>
        );
      })}
    </div>
  );
};
