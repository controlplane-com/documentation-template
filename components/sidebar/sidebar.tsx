import * as React from "react";
import { NavLink } from "../navlink/navlink";
import { Scrollbars } from "react-custom-scrollbars";
import style from "./sidebar.module.scss";
import { sidebar } from "../../sidebar";
import clsx from "clsx";
import { SubNavLink } from "../navlink/subnavlink";

interface Props {
  isOpenAtMobile: boolean;
  closeSidebar: () => void;
}
export const Sidebar: React.FC<Props> = ({ isOpenAtMobile, closeSidebar }) => {
  const [collapsedLabels, setCollapsedLabels] = React.useState<string[]>(
    sidebar.filter((s) => s.expanded === false).map((s) => s.label)
  );

  function toggleCollapsed(label: string) {
    if (collapsedLabels.includes(label)) {
      setCollapsedLabels(collapsedLabels.filter((x) => x !== label));
    } else {
      setCollapsedLabels([...collapsedLabels, label]);
    }
  }

  function expandLabel(label: string) {
    setCollapsedLabels(collapsedLabels.filter((x) => x !== label));
  }

  return (
    <div className={clsx(style.sidebar, isOpenAtMobile && style.sidebarMobile)}>
      <Scrollbars
        thumbMinSize={30}
        universal={true}
        style={{ width: "100%", height: "100%" }}
      >
        {sidebar.map((navLink) => {
          return (
            <div key={navLink.label}>
              <NavLink
                navLink={navLink}
                onClick={navLink.url ? closeSidebar : () => {}}
                isCollapsed={collapsedLabels.includes(navLink.label)}
                toggleCollapsed={() => toggleCollapsed(navLink.label)}
              />
              {navLink.subitems?.map((subNavLink) => (
                <SubNavLink
                  key={subNavLink.label}
                  subNavLink={subNavLink}
                  onClick={closeSidebar}
                  isCollapsed={collapsedLabels.includes(navLink.label)}
                  expandLabel={() => expandLabel(navLink.label)}
                />
              ))}
            </div>
          );
        })}
      </Scrollbars>
    </div>
  );
};
