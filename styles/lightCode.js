export default {
  'code[class*="language-"]': {
    color: "rgb(57, 58, 52)",
    background: "none",
    fontFamily: "SFMono",
    fontSize: "13.333px",
    textAlign: "left",
    whiteSpace: "pre",
    wordSpacing: "normal",
    wordBreak: "normal",
    wordWrap: "normal",
    lineHeight: "1.5",
    MozTabSize: "4",
    OTabSize: "4",
    tabSize: "4",
    WebkitHyphens: "none",
    MozHyphens: "none",
    msHyphens: "none",
    hyphens: "none",
    maxHeight: "inherit",
    height: "inherit",
    padding: "0",
    display: "block",
    width: "100%",
    overflow: "visible",
  },
  'pre[class*="language-"]': {
    color: "black",
    background: "none",
    fontFamily: "SFMono",
    fontSize: "13.333px",
    textAlign: "left",
    whiteSpace: "pre",
    wordSpacing: "normal",
    wordBreak: "normal",
    wordWrap: "normal",
    lineHeight: "1.5",
    MozTabSize: "4",
    OTabSize: "4",
    tabSize: "4",
    WebkitHyphens: "none",
    MozHyphens: "none",
    msHyphens: "none",
    hyphens: "none",
    position: "relative",
    margin: ".5em 0",
    padding: "16px",
    backgroundColor: "rgb(246, 248, 250)",
    WebkitBoxSizing: "border-box",
    MozBoxSizing: "border-box",
    boxSizing: "border-box",
    marginBottom: "1em",
    overflow: "auto",
  },
  'pre[class*="language-"]>code': {
    position: "relative",
    borderLeft: "10px solid #358ccb",
    boxShadow: "-1px 0px 0px 0px #358ccb, 0px 0px 0px 1px #dfdfdf",
    backgroundColor: "rgb(246, 248, 250)",
    backgroundImage:
      "linear-gradient(transparent 50%, rgba(69, 142, 209, 0.04) 50%)",
    backgroundSize: "3em 3em",
    backgroundOrigin: "content-box",
    backgroundAttachment: "local",
    width: "100%",
  },
  ':not(pre) > code[class*="language-"]': {
    backgroundColor: "rgb(246, 248, 250)",
    WebkitBoxSizing: "border-box",
    MozBoxSizing: "border-box",
    boxSizing: "border-box",
    marginBottom: "1em",
    position: "relative",
    padding: ".2em",
    borderRadius: "0.3em",
    color: "rgb(54, 172, 170)",
    border: "1px solid rgba(0, 0, 0, 0.1)",
    display: "inline",
    whiteSpace: "normal",
  },
  'pre[class*="language-"]:before': {
    content: "''",
    zIndex: "-2",
    display: "block",
    position: "absolute",
    bottom: "0.75em",
    left: "0.18em",
    width: "40%",
    height: "20%",
    maxHeight: "13em",
    boxShadow: "0px 13px 8px #979797",
    WebkitTransform: "rotate(-2deg)",
    MozTransform: "rotate(-2deg)",
    msTransform: "rotate(-2deg)",
    OTransform: "rotate(-2deg)",
    transform: "rotate(-2deg)",
  },
  'pre[class*="language-"]:after': {
    content: "''",
    zIndex: "-2",
    display: "block",
    position: "absolute",
    bottom: "0.75em",
    left: "auto",
    width: "40%",
    height: "20%",
    maxHeight: "13em",
    boxShadow: "0px 13px 8px #979797",
    WebkitTransform: "rotate(2deg)",
    MozTransform: "rotate(2deg)",
    msTransform: "rotate(2deg)",
    OTransform: "rotate(2deg)",
    transform: "rotate(2deg)",
    right: "0.75em",
  },
  comment: {
    color: "#7D8B99",
    fontStyle: "italic",
  },
  "block-comment": {
    color: "#7D8B99",
  },
  prolog: {
    color: "#7D8B99",
  },
  doctype: {
    color: "#7D8B99",
  },
  cdata: {
    color: "#7D8B99",
  },
  punctuation: {
    color: "#5F6364",
  },
  property: {
    color: "rgb(54, 172, 170)",
  },
  tag: {
    color: "rgb(54, 172, 170)",
  },
  boolean: {
    color: "rgb(54, 172, 170)",
  },
  number: {
    color: "rgb(54, 172, 170)",
  },
  "function-name": {
    color: "rgb(54, 172, 170)",
  },
  constant: {
    color: "rgb(54, 172, 170)",
  },
  symbol: {
    color: "rgb(54, 172, 170)",
  },
  deleted: {
    color: "rgb(54, 172, 170)",
  },
  selector: {
    color: "rgb(215, 58, 73)",
  },
  "attr-name": {
    color: "rgb(215, 58, 73)",
  },
  string: {
    color: "rgb(215, 58, 73)",
  },
  char: {
    color: "rgb(215, 58, 73)",
  },
  function: {
    color: "rgb(215, 58, 73)",
  },
  builtin: {
    color: "rgb(215, 58, 73)",
  },
  inserted: {
    color: "rgb(215, 58, 73)",
  },
  operator: {
    color: "#a67f59",
    background: "rgba(255, 255, 255, 0.5)",
  },
  entity: {
    color: "#a67f59",
    background: "rgba(255, 255, 255, 0.5)",
    cursor: "help",
  },
  url: {
    color: "#a67f59",
    background: "rgba(255, 255, 255, 0.5)",
  },
  variable: {
    color: "#a67f59",
    background: "rgba(255, 255, 255, 0.5)",
  },
  atrule: {
    color: "rgb(0, 164, 219)",
  },
  "attr-value": {
    color: "rgb(0, 164, 219)",
  },
  keyword: {
    color: "rgb(0, 164, 219)",
  },
  "class-name": {
    color: "rgb(0, 164, 219)",
  },
  regex: {
    color: "#e90",
  },
  important: {
    color: "#e90",
    fontWeight: "normal",
  },
  ".language-css .token.string": {
    color: "#a67f59",
    background: "rgba(255, 255, 255, 0.5)",
  },
  ".style .token.string": {
    color: "#a67f59",
    background: "rgba(255, 255, 255, 0.5)",
  },
  bold: {
    fontWeight: "bold",
  },
  italic: {
    fontStyle: "italic",
  },
  namespace: {
    Opacity: ".7",
  },
  'pre[class*="language-"].line-numbers.line-numbers': {
    paddingLeft: "0",
  },
  'pre[class*="language-"].line-numbers.line-numbers code': {
    paddingLeft: "3.8em",
  },
  'pre[class*="language-"].line-numbers.line-numbers .line-numbers-rows': {
    left: "0",
  },
  'pre[class*="language-"][data-line]': {
    paddingTop: "0",
    paddingBottom: "0",
    paddingLeft: "0",
  },
  "pre[data-line] code": {
    position: "relative",
    paddingLeft: "4em",
  },
  "pre .line-highlight": {
    marginTop: "0",
  },
};
