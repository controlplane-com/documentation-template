import "./../styles/global.css";
import "./../styles/markdown.css";
import "./../styles/tailwind.css";
import firebase from "firebase/app";
import "firebase/analytics";
import type { AppProps } from "next/app";
import Head from "next/head";
import Router from "next/router";
import { Layout } from "./../components/layout/layout";
import MDXProvider from "./../components/MDXProvider";
import * as React from "react";
import { NextPrevButtons } from "components/nextPrevButtons";
import { useRouter } from "next/router";
import { sidebar } from "./../sidebar";

const firebaseConfig = {
  apiKey: "FROM_FIREBASE",
  authDomain: "FROM_FIREBASE",
  projectId: "FROM_FIREBASE",
  appId: "FROM_FIREBASE",
  measurementId: "FROM_FIREBASE",
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}
if (typeof window !== "undefined") {
  firebase.analytics();
}

function sendAnalyticOnRouteChange(url: any) {
  let title = "";
  try {
    title = (document.querySelector("head > title") as any)?.innerText;
  } catch (e) {
    title = "Title not found";
  }
  firebase.analytics().logEvent("page_view", {
    page_location: url,
    page_path: Router.asPath,
    page_title: title,
  });
}

Router.events.on("routeChangeComplete", (url) => {
  if (typeof window !== "undefined") {
    sendAnalyticOnRouteChange(url);
  }
});

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  const [title, setTitle] = React.useState(
    "DEFAULT_TITLE"
  );

  const timeoutRef = React.useRef<any>(null);
  React.useEffect(() => {
    // includes # values too
    const headings = document.querySelectorAll(
      ".markdown > h1, .markdown > h2, .markdown > h3"
    );
    const onScroll = () => {
      let isUpdated = false;
      headings.forEach((heading) => {
        const rect = heading.getBoundingClientRect();
        if (rect.top > 0 && rect.top < 150 && !isUpdated) {
          const location = window.location.toString().split("#")[0];
          // @ts-ignore
          window.history.replaceState(
            // @ts-ignore
            null,
            // @ts-ignore
            null,
            // @ts-ignore
            location + "#" + heading.id
          );
          isUpdated = true;
        }
      });
    };
    timeoutRef.current = setTimeout(() => {
      document.addEventListener("scroll", onScroll);
      timeoutRef.current = null;
    }, 500);

    return () => {
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current);
        timeoutRef.current = null;
      } else {
        document.removeEventListener("scroll", onScroll);
      }
    };
  }, [router.asPath]);

  React.useEffect(() => {
    // stripped out of # values

    // for updating title
    const item = sidebar.find((i) => i.url === router.pathname);
    if (item) {
      // @ts-ignore
      setTitle(`${item.title || item.label} - APPEND_TO_TITLE`);
    }
  }, [router.pathname]);

  return (
    <MDXProvider>
      <Head>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <title key="title">{title}</title>
        <meta http-equiv="Content-Type" content="text/html;" charSet="utf-8" />
        <meta
          name="description"
          content="DESCRIPTION"
        />
        <meta
          name="keywords"
          content="KEYWORDS"
        />
        <meta name="author" content="COMPANY_NAME" />
      </Head>
      <Layout>
        <Component {...pageProps} />
        <NextPrevButtons />
      </Layout>
    </MDXProvider>
  );
}

export default MyApp;
