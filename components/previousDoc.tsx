import Link from "next/link";
import { NextPrevData } from "types";
import { ChevronsLeft } from "react-feather";

interface Props {
  link: NextPrevData | null;
}
export const PreviousDoc: React.FC<Props> = ({ link }) => {
  if (!link) {
    return (
      <div className="tw-prev-placeholder">
        <div className="invisible">placeholder</div>
      </div>
    );
  }
  return (
    <Link href={link.url}>
      <div className="tw-next-prev-button tw-prev">
        <span>Previous</span>
        <a>
          <ChevronsLeft className="inline" style={{ width: 15, height: 15 }} />
          {link.label}
        </a>
      </div>
    </Link>
  );
};
