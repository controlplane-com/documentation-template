# !/bin/bash

cat << EOF >> ./algolia.env
APPLICATION_ID=$1
API_KEY=$2
EOF

docker run --env-file=algolia.env -e "CONFIG=$(cat ./config.json | jq -r tostring)" algolia/docsearch-scraper