import React from "react";
import Highlight, { defaultProps } from "prism-react-renderer";
import github from "prism-react-renderer/themes/github";

interface Props {
  className: string;
  title: string;
  noCopy: string;
  metastring: string;
}
export const PrismCodeHighlighter: React.FC<Props> = ({
  children,
  className = "",
  noCopy = false,
  metastring,
}) => {
  const [isCopied, setIsCopied] = React.useState(false);

  const language = className.replace(/language-/, "");
  const hasTitle = metastring && metastring.includes("title=");

  React.useEffect(() => {
    let timeout: any = null as any;
    if (isCopied) {
      timeout = setTimeout(() => {
        setIsCopied(false);
        timeout = null;
      }, 2000);
    }

    return () => {
      if (timeout) {
        clearTimeout(timeout);
      }
    };
  }, [isCopied]);

  let title = "";
  if (hasTitle) {
    title = metastring.split("title=")[1];
    if (title.startsWith('"') && title.endsWith('"')) {
      title = title.slice(1, title.length - 1);
    }
  }

  return (
    <div className="relative tw-code">
      {hasTitle ? (
        <div
          className="font-bold border-b border-gray-300 whitespace-normal	"
          style={{ padding: 16, color: "rgb(57, 58, 52)" }}
        >
          {title}
        </div>
      ) : null}
      <div className="relative">
        {noCopy ? null : (
          <div
            className="copy absolute bg-gray-600 text-gray-100 px-2 py-1 text-xs rounded cursor-pointer uppercase tracking-wider"
            style={{ right: 15, top: 15 }}
            onClick={() => {
              if (typeof window !== "undefined") {
                // @ts-ignore
                window.navigator.clipboard.writeText(children.toString());
              }
              setIsCopied(true);
            }}
          >
            {isCopied ? "copied" : "copy"}
          </div>
        )}
        {/* @ts-ignore */}
        <Highlight
          {...defaultProps}
          theme={github}
          code={children}
          language={language}
        >
          {({ className, style, tokens, getLineProps, getTokenProps }) => (
            <pre className={className} style={{ ...style, padding: "20px" }}>
              {tokens.map((line, i) => {
                if (
                  i === tokens.length - 1 &&
                  line.length === 1 &&
                  !!line[0].empty
                ) {
                  return null;
                }
                return (
                  <div key={i} {...getLineProps({ line, key: i })}>
                    {line.map((token, key) => {
                      const tokenProps = getTokenProps({ token, key });
                      if (tokenProps.children.startsWith("cpln ")) {
                        return (
                          <div key={key}>
                            <span
                              className="token function"
                              style={{ color: "#d73a49" }}
                            >
                              cpln{" "}
                            </span>
                            <span {...tokenProps}>
                              {tokenProps.children.split("cpln ")[1]}
                            </span>
                          </div>
                        );
                      }
                      return (
                        <span key={key} {...getTokenProps({ token, key })} />
                      );
                    })}
                  </div>
                );
              })}
            </pre>
          )}
        </Highlight>
      </div>
    </div>
  );
};
