import Link from "next/link";
import { NextPrevData } from "types";
import { ChevronsRight } from "react-feather";

interface Props {
  link: NextPrevData | null;
}
export const NextDoc: React.FC<Props> = ({ link }) => {
  if (!link) {
    return (
      <div className="tw-next-placeholder">
        <div className="invisible">placeholder</div>
      </div>
    );
  }
  return (
    <Link href={link.url}>
      <div className="tw-next-prev-button tw-next">
        <span>Next</span>
        <a>
          {link.label}
          <ChevronsRight className="inline" style={{ width: 15, height: 15 }} />
        </a>
      </div>
    </Link>
  );
};
