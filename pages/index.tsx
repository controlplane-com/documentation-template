import { useRouter } from "next/router";
import * as React from "react";

const HomePage: React.FC = () => {
  const router = useRouter();

  return (
    <div>
      <div>index - {router.asPath}</div>
    </div>
  );
};

export default HomePage;
