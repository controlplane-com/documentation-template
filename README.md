# Documentation Template

View the raw README.md file to see the examples below without GitLab formatting. 

## Prerequisites

- Example is hosted at Firebase, but can be hosted at any provider.
  - Configure project at [Firebase](https://firebase.google.com/). Add any custom domain, etc.
  - Install the Firebase Tools [CLI](https://github.com/firebase/firebase-tools).
  - Example contains references to two firebase projects; TEST and PROD which can be mapped to two repo branches, test and main.
  - See `Deploy Site` section below for additional details.

- Update the following:
  - In file: `./next-sitemap-config.js`, update `siteUrl`. It can also be the value of the the `SITE_URL` env variable as part of a CI/CD pipeline.
  - Override the default page by updating the destination property in the file: `./next.config.js`.
  - Override the default page for the two Firebase projects by updating the destination property in the file: `./firebase.json`.
  - Update the following in the file: `./components/layout/layout.tsx` which contains the header and footer:
    - Lines 60-63: From Algolia.
    - Line 69, 95, 106, 140.
    - Remove any unnecessary elements.
    - Line 93 links to the header logo. Update logo images at `./public/img/logo.svg` and `./public/img/favicon.png`.
  - Update the following in the file: `./pages/_app.tsx`:
    - Lines 54, 104, 116, 120, and 122.


- Search:
  - Provided by [Algolia](https://algolia.com).
  - Apply at: https://docsearch.algolia.com/.
  - The files within the `./algolia` directory are used by the CI/CD pipeline to execute the Algolia crawler.
  - The file `./algolia/config.json` is provided by Algolia after registration.
  - `jq` is required to be installed.
  - The `./algolia/crawl.sh` script requires the Algolia application and API key.
    - Example: `./crawl.sh ${ALGOLIA_APPLICATION_ID} ${ALGOLIA_API_KEY}`
    - The crawl is typically executed after changes are made to the documentation to force Algolia to crawl the new changes. Otherwise, they automatically crawl randomly every 24 hours.

- Node.js
  - Execute `npm i` to install all the node packages.


## Local Testing

1. Execute `npm run dev` to start the local dev server.
2. Browse to `http://localhost:3000`. Changes will be automatically rendered. Changes to the left or right tables might require a full page refresh.

## Deploy Site

- Example is hosted at Firebase.
  - Create a Firebase hosting project to obtain the required ID and tokens.
  - Update the `firebaseConfig` in the file `./_app.tsx`.
  - Update the default project name in the file: `./.firebaserc`.
  - To deploy to Firebase:
    - Execute: `npm run export` to generate the static files.
    - Execute: `npx firebase deploy --token $FIREBASE_TOKEN --only hosting:FIREBASE_PROJECT_NAME` to deploy to Firebase.

## About.js

The file `./about.js` will add a revision tag to the end of the copyright at the bottom of each page.

## Site Map

The site map will automatically be generated and saved at `./public/sitemap.xml`.

## Sample CI/CD Pipeline

The example includes a sample GitLab pipeline example in the file: `./.gitlab-ci.yml`.


## Left Side - Table of Contents

Updated by modifying the `sidebar.ts` file. See currently file for examples.


## Right Side - Table of Contents

Generated based on header (#) titles. Sub-elements are generated for each header level (##, ###, etc.).

- To exclude a header from the ToC, use `--exclude`
  
```
## Header content {url} --exclude
```

## Code Highlighting

Use language name to highlight code blocks.

Example:

```js

function test() {


}

```


To Remove copy button:

Example:

```js NoCopy NoCopy

Hello World

```

To add a title:

```js NoCopy title="Title for Code Example"

Hello World!

```


## Admonitions

Available types:

- note 
- tip 
- info 
- warning 
- caution


Example:

```

<admonition type="warning">

This is a WARNING!

</admonition>

```


