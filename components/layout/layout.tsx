import * as React from "react";
import style from "./layout.module.scss";
import clsx from "clsx";
import { Menu as FeatherMenu, X as FeatherClose } from "react-feather";
import { useRouter } from "next/router";
import { TableOfContents } from "../tableOfContents/tableOfContents";
import { Sidebar } from "../sidebar/sidebar";

export const Layout: React.FC = ({ children }) => {
  const router = useRouter();
  const [isSidebarOpen, setIsSidebarOpen] = React.useState(false);
  const [isTocOpen, setIsTocOpen] = React.useState(false);
  const [about, setAbout] = React.useState<any>();

  React.useEffect(() => {
    fetch("/about.json")
      .then((res) => res.json())
      .then((res) => setAbout(res));
    const onResize = () => {
      const { matches: isComputer } = window.matchMedia("(min-width: 1280px)");
      if (isComputer) {
        setIsTocOpen(false);
        setIsSidebarOpen(false);
      }
    };
    window.addEventListener("resize", onResize);
    return () => {
      window.removeEventListener("resize", onResize);
    };
  }, []);

  React.useEffect(() => {
    setIsSidebarOpen(false);
    setIsTocOpen(false);
  }, [router.asPath]);

  React.useEffect(() => {
    const cb = () => {
      setIsTocOpen((x) => !x);
    };
    try {
      const oldMobileToc: HTMLElement = document.querySelector(
        ".markdown > h1 > svg.mobile-toc"
      ) as HTMLElement;
      const newMobileToc = oldMobileToc.cloneNode(true);
      oldMobileToc.parentNode?.replaceChild(newMobileToc, oldMobileToc);
      newMobileToc.addEventListener("click", cb);
    } catch (e) {}
  }, [router.pathname]);

  React.useEffect(() => {
    document.body.style.overflow = isTocOpen ? "hidden" : "auto";
  }, [isTocOpen]);

  React.useEffect(() => {
    // @ts-ignore
    if (window.docsearch) {
      // @ts-ignore
      window.docsearch({
        appId: "FROM_ALGOLIA",
        apiKey: "FROM_ALGOLIA",
        indexName: "FROM_ALGOLIA",
        inputSelector: ".tw-algolia-search-input",
        transformData: (hits: any) => {
          for (let hit of hits) {
            let url = hit.url;
            if (process.env.NODE_ENV === "development") {
              url = url.replace(
                "https://docs.example.com/",
                "http://localhost:3000/"
              );
            }
            hit.url = url;
          }
        },
        debug: process.env.NODE_ENV === "development",
      });
    } else {
      console.warn("Search has failed to load");
    }
  }, []);

  function toggleSidebar() {
    setIsSidebarOpen((x) => !x);
  }

  return (
    <div className={style.layout}>
      <div className={style.header}>
        <div className="tw-header">
          <img
            className="cursor-pointer"
            src={"/img/logo.svg"}
            onClick={() => {
              window.open(`https://example.com`, "_blank");
            }}
          />
          <div className="flex-grow" />
          <input
            className={`tw-algolia-search-input`}
            placeholder={"search"}
            style={{ fontSize: 16 }}
          />
          <a
            className={`${style["custom-button"]} hidden xl:flex`}
            href={`https://custom.link.example.com`}
            target={"_blank"}
          >
            Console
          </a>
        </div>
      </div>
      <div className={style.sidebarAndDocPageAndToc}>
        <div
          className={clsx(style.sidebar, isSidebarOpen && style.sidebarMobile)}
        >
          <Sidebar
            isOpenAtMobile={isSidebarOpen}
            closeSidebar={() => setIsSidebarOpen(false)}
          />
        </div>
        {isTocOpen ? (
          <div
            onClick={() => setIsTocOpen(false)}
            className={style.tableOfContentsMobileButton}
          >
            <FeatherClose />
          </div>
        ) : (
          <div onClick={toggleSidebar} className={style.sidebarMobileButton}>
            {isSidebarOpen ? <FeatherClose /> : <FeatherMenu />}
          </div>
        )}
        <div className={style.docPageAndToc}>
          <div className={style.markdownAndFooter}>
            <div className={clsx(style.markdown, "markdown")}>{children}</div>
            <div
              className={`px-8 py-4 text-gray-600 border-t border-gray-200 flex flex-col`}
            >
              Copyright © 2022 Example Corporation. All rights reserved.
              Revision{" "}
              {about?.version || process.env.REACT_APP_CI_COMMIT_SHORT_SHA}
            </div>
          </div>
          <div
            className={clsx(
              style.tableOfContents,
              isTocOpen && style.tableOfContentsMobile
            )}
          >
            <TableOfContents isOpenAtMobile={isTocOpen} />
          </div>
        </div>
      </div>
    </div>
  );
};
