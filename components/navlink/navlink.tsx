import * as React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import clsx from "clsx";
import type { NavLink as TNavLink } from "types";
import { ChevronDown, ChevronUp } from "react-feather";

interface Props {
  navLink: TNavLink;
  onClick: () => void;
  isCollapsed: boolean;
  toggleCollapsed: () => void;
}
export const NavLink: React.FC<Props> = ({
  navLink,
  onClick,
  isCollapsed,
  toggleCollapsed,
}) => {
  const { url, label } = navLink;
  const router = useRouter();

  const isActive = router.asPath.split("#")[0] === url;

  function localOnClick() {
    toggleCollapsed();
    onClick();
  }

  if (url) {
    return (
      <Link href={url}>
        <a
          onClick={onClick}
          className={clsx("tw-nav-link", { "tw-nav-link-active": isActive })}
        >
          {label}
        </a>
      </Link>
    );
  }

  return (
    <div
      onClick={localOnClick}
      className={clsx("tw-nav-link", "flex justify-between items-center")}
    >
      <span>{label}</span>
      {isCollapsed ? (
        <ChevronDown className="cursor-pointer" />
      ) : (
        <ChevronUp className="cursor-pointer" />
      )}
    </div>
  );
};
